/**
  ******************************************************************************
  * @file    	control.h 
  * @author  	Philipp Schwarz
  *        	    HAW-Hamburg
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2018-05-15
  * @brief   Main program body
  ******************************************************************************
  */
  
#include <stdint.h> // for standarized integers
#include <stdbool.h> // for booleans

/**
 * Calculates the arrays crc and checks it against the last byte which is the crc
 * @param array The array we are checking
 * @param size The size of the array
 * @return a bool, true if the data is correct and false if it is not
 */
bool check_crc(uint8_t* array, int size);

/**
 * Automaticaly detects all conected sensors and put out their temperature cycly
 */
void detect_sensors_and_mesaure_temperature(void);

/**
 * Measure the temperature of a sensor when it is the only one connected
 */
void measure_temperature_one_sensor(void);

/**
 * Measures the temperature of the specificed sensor
 * @param rom The rom of the sensor we read the temperature of
 */
void measure_temperature_specific_sensor(unsigned char* rom);

/**
 * Measure the temperature via the four specified roms of the simulation
 */
void measure_temperature_specific_simulation_sensors(void);

/**
 * Initializes the search for sensor roms
 */
void first(void);

/**
 * Writes the next found rom into the roms array of does nothing if the last_device_flag is set
 * @return a bool if the search was successfull
 */
bool next(void);

/**
 * Reads the rom of a sensor if it is the only one connected
 */
void read_rom(void);

/**
 * The superloop that executes the other functions
 */
void superloop(void);

/**
 * Lets the program idle for the specified time in microseconds
 * @param t The idle time in ms
 */
void wait(int t);
