/**
  ******************************************************************************
  * @file    	one-wire.c 
  * @author  	Philipp Schwarz
  *        	    HAW-Hamburg
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2018-05-15
  * @brief   Main program body
  ******************************************************************************
  */
  
#include "TI_memory_map.h" // for GPIO

#include "control.h" // for wait()
#include "one-wire.h" // for the voltage enum
#include <stdint.h> // for standarized integers

#define PIN_0 0 // push-pull
#define PIN_1 1 // open-drain

#define MASK_PIN_0 (0x01U << PIN_0)
#define MASK_PIN_1 (0x01U << PIN_1)

void set_mode_open_drain(uint8_t pin) { // without internal pull-up, relying on external resistor
    GPIOG->OTYPER |= (0x1U << pin); // set pin to open-drain
}

void set_pin_voltage(voltage v, uint8_t pin) {
    if (v == HIGH) {
        GPIOG->BSRRL = (0x1U << pin); // set voltage to high
    } else if (v == LOW) {
        GPIOG->BSRRH = (0x1U << pin); // set voltage to low
    }
}

void set_mode_push_pull(uint8_t pin) {
    GPIOG->OTYPER &= ~(0x1U << pin);
    set_pin_voltage(HIGH, pin);
}

unsigned char get_bus_state() {
    return (GPIOG->IDR & MASK_PIN_0) != 0;
}

void write_bit(unsigned char bit) {
    if (bit == 1) {
        set_pin_voltage(LOW, 0);
        wait(6);
        set_pin_voltage(HIGH, 0);
        wait(64);
    } else if (bit == 0) {
        set_pin_voltage(LOW, 0);
        wait(60);
        set_pin_voltage(HIGH, 0);
        wait(10);
    }
}

void write_byte(unsigned char byte) {
    unsigned char bit = 0;
    
    for (unsigned char i = 0; i < 8; i++) {
        bit = byte >> i;
        bit &= 0x01; // set every character higher than the lsb to 0
        write_bit(bit);
    }
}

unsigned char read_bit() {
    set_pin_voltage(LOW, 0);
    wait(6);
    set_pin_voltage(HIGH, 0);
    wait(9);
    unsigned char bus_state = get_bus_state();
    wait(55);
    return bus_state;
}

unsigned char read_byte() {
    unsigned char byte = 0;
    
    for (unsigned char i = 0; i < 8; i++) {
        byte |= (read_bit() << i);
    }
    
    return byte;
}

unsigned char reset_bus() {
    set_pin_voltage(LOW, 0);
    wait(480);
    set_pin_voltage(HIGH, 0);
    wait(70);
    unsigned char bus_state = get_bus_state();
    wait(410);
    return bus_state;
}
