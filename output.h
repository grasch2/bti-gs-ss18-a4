/**
  ******************************************************************************
  * @file    	output.h 
  * @author  	Philipp Schwarz
  *        	    HAW-Hamburg
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2018-05-15
  * @brief   Main program body
  ******************************************************************************
  */

#include <stdint.h> // for standarized integers

/**
 * Displays an error message
 * @param e The error code
 */
void display_error_message(int e);

/**
 * Displays rom data
 * @param rom The rom that will be displayed
 */
void display_rom_data(uint8_t* rom);

/**
 * Displays the temperature
 * @param t1 The more significant byte of the temperature bytes of the scratchpad
 * @param t2 The less significant byte of the temperatues bytes of the scratchpad containing the decimals
 */
void display_temperature(uint8_t t1, uint8_t t2);
