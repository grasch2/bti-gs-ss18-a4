/**
  ******************************************************************************
  * @file    	control.c 
  * @author  	Philipp Schwarz
  *        	    HAW-Hamburg
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2018-05-15
  * @brief   Main program body
  ******************************************************************************
  */
  
#include "TI_memory_map.h" // for the LEDs
#include <timer.h>
#include "control.h" // for booleans
#include "output.h"
#include "one-wire.h"
#include "TI_Lib.h" //for TFT controls
#include <stdio.h>

#define READ_ROM 0x33
#define SKIP_ROM 0xCC
#define MATCH_ROM 0x55
#define CONVERT_T 0x44
#define READ_SCRATCHPAD 0xBE
#define SEARCH_ROM 0xF0

#define FOUND_SENSOR 0
#define FOUND_NO_SENSOR 1
#define ROM_SIZE 8
#define SCRATCHPAD_SIZE 9
#define MAX_SENSORS 4
#define FAMILY_CODE 0x28
#define DELAY_RATE 2000000 // micro sec

static uint8_t roms[MAX_SENSORS][ROM_SIZE] = {0}; // contains all the roms we found
static uint8_t last_discrepancy = 0; // bit index that identifies from which bit the next search discrepancy check should start
static uint8_t last_family_discrepancy = 0; // bit index that identifies the last discrepancy in the family code byte
static bool last_device_flag = true; // flag that indicates if we found the last device on the bus
static uint8_t ROM_NO[ROM_SIZE] = {0}; // buffer were we note the rom of the current device
static uint8_t roms_index = 0; // index that counts how many roms we found

void wait(int t) {
    uint32_t time = t * CONVERT2US + getTimeStamp();
    while (getTimeStamp() < time) {};
}

bool check_crc(uint8_t* array, int size) {
    uint8_t crc_calculated = 0;

    for (int i = 0; i < size-1; i++) { // iterate throug the array except the last value since it's the crc
        uint8_t current_byte = array[i];
			
        for (int j = 0; j < 8; j++) {
            bool is_special = (crc_calculated ^ current_byte) & 0x01; // check if crc_calulated xored with current_byte has a 1 as it's lsb
            crc_calculated >>= 1; // shift crc_calculated one bit right
            if (is_special) { // if the statement from above is true xor crc_calculated wiht 0x8C
                crc_calculated ^= 0x8C;
            }
            current_byte >>= 1; // shift the current byte 1 times right
        }			
    }
    if (crc_calculated == array[size-1]) { // check the calculated crc with the recieved one
        return true;
    } else {
        return false;
    }
}

void read_rom() { // step 1
    uint8_t rom[ROM_SIZE];
    
    int e = reset_bus();
    if (e == FOUND_SENSOR) { // if we found atleast one sensor
        write_byte(READ_ROM); 
        for (int i = 0; i < ROM_SIZE; i++) {
            if (i == 0) {
            GPIOG -> ODR = GPIOG -> IDR | (0x1U << 3); // turn LED 3 on
            }
            rom[i] = read_byte(); // read the bytes into the array
            if (i == 0) {
            GPIOG -> ODR = GPIOG -> IDR & ~(0x1U << 3);  // turn LED 3 off
            }
        }
        
        
        if (check_crc(rom, ROM_SIZE)) {
            display_rom_data(rom);
        } else {
            display_error_message(-1);
        }
    } else if (e == FOUND_NO_SENSOR) { // if we found no sensors
        display_error_message(-2);
    } else {
        display_error_message(-99);
    }
}

void measure_temperature_one_sensor() { // step 2.1
    uint8_t scratchpad[SCRATCHPAD_SIZE];
    
    int e = reset_bus();
    if (e == FOUND_SENSOR) { // if we found atleast one sensor
        write_byte(SKIP_ROM); // say that we don't want to check roms
        write_byte(CONVERT_T); // say that we want to measure the temperature
        set_mode_push_pull(0); // do the measurement
        wait(750000);
        set_mode_open_drain(0);
        reset_bus();
        write_byte(SKIP_ROM);  // say that we don't want to check roms
        write_byte(READ_SCRATCHPAD); // say that we want to read the scratchpad
        
        for (int i = 0; i < SCRATCHPAD_SIZE; i++) {
            scratchpad[i] = read_byte(); // read the scratchpad into the array
        }
        
        if (check_crc(scratchpad, SCRATCHPAD_SIZE)) {
            TFT_cls();
            display_temperature(scratchpad[1], scratchpad[0]); // display both temperature relevant bytes from the scratchpad
            //wait(DELAY_RATE);
        } else {
            display_error_message(-1);
        }
    } else if (e == FOUND_NO_SENSOR) { // if we found no sensors
        display_error_message(-2);
    } else {
        display_error_message(-99);
    }
}

void measure_temperature_specific_sensor(unsigned char* rom) {
    uint8_t scratchpad[SCRATCHPAD_SIZE];
   
    int e = reset_bus();
    if (e == FOUND_SENSOR) {
        write_byte(MATCH_ROM); // say that we want a specific sensor
        for (int i = 0; i < ROM_SIZE; i++) { // write a rom from the table to get the specific sensor
            write_byte(rom[i]);
        }
        write_byte(CONVERT_T); // say that we want to measure the temperature
        set_mode_push_pull(0);
        wait(750000);
        set_mode_open_drain(0);
        reset_bus();
        write_byte(MATCH_ROM); // say that we want a specific sensor
        for (int i = 0; i < ROM_SIZE; i++) { // write a rom from the table to get the specific sensor
            write_byte(rom[i]);
        }
        write_byte(READ_SCRATCHPAD); // say that we want to read the scratchpad
         
        for (int i = 0; i < SCRATCHPAD_SIZE; i++) {
            scratchpad[i] = read_byte(); // read the scratchpad into the array
        }

        if (check_crc(scratchpad, SCRATCHPAD_SIZE)) {
            display_temperature(scratchpad[1], scratchpad[0]); // display both temperature relevant bytes from the scratchpad
            //wait(DELAY_RATE);
        } else {
            display_error_message(-1);
        }
    } else if (e == FOUND_NO_SENSOR) { // if we found no sensors
        display_error_message(-2);
    } else {
        display_error_message(-99);
    }
}

void measure_temperature_specific_simulation_sensors() { // step 2.2
    // roms are from simulation
    unsigned char roms[MAX_SENSORS][ROM_SIZE] = {{0x28, 0xa0, 0x90, 0xe6, 0x01, 0x00, 0x00, 0x1e},
                                                 {0x28, 0xf2, 0x73, 0xe6, 0x01, 0x00, 0x00, 0xca},
                                                 {0x28, 0x3e, 0x88, 0xe6, 0x01, 0x00, 0x00, 0xfe},
                                                 {0x28, 0x61, 0x82, 0xe6, 0x01, 0x00, 0x00, 0x49}};
    
    for (int i = 0; i < 4; i++) {
        measure_temperature_specific_sensor(roms[i]);
		TFT_newline();
		TFT_carriage_return();
    }
	wait(DELAY_RATE);
	TFT_cls();
	TFT_gotoxy(1,1);
}

void first() {
    // reset all global variables
    last_discrepancy = 0;
    last_device_flag = false;
    last_family_discrepancy = 0;
    roms_index = 0;
    
    // reset the roms array
    for (int i = 0; i < MAX_SENSORS; i++) {
        for (int j = 0; j < ROM_SIZE; j++) {
            roms[i][j] = 0;
        }
    }
}

bool next() {
    uint8_t last_zero = 0; // 4; bit position of the last zero written where there was a discrepancy
    uint8_t rom_byte_number = 0; // the current byte of the current sensor we are searching
    uint8_t id_bit = 0; // the first bit in a bit search sequence
    uint8_t cmp_id_bit = 0; // the complement of id_bit
    uint8_t search_direction = 0; // bit value indicating the direction of the search, all devices with this bit stay in the search and the rest go into a wait state for a reset

    uint8_t byte = 0; // a byte where we note all the bits of the current search
    uint8_t bit_position = 0; // the current bit position we are at in the byte
    
    printf("Sensor Nr. %d\n", roms_index);
    
    // 1
    if (reset_bus() == FOUND_NO_SENSOR) { // 2; if there is no sensor connected reset the search
        // 26
        last_discrepancy = 0;
        last_device_flag = false;
        last_family_discrepancy = 0;
		
		display_error_message(-2); // show an error message
        return false; // 27
    }
    // 3
    if (!last_device_flag) { // only search if we did not already add the rom of the last device
        write_byte(SEARCH_ROM);
        // 4; 18
        for (int id_bit_number = 1; id_bit_number <= ROM_SIZE * 8; id_bit_number++) { // id_bit_number is the current overall bit of the rom we are at
            printf("Bitnummer: %d\n", id_bit_number);
            
            // 6
            id_bit = read_bit();
            cmp_id_bit = read_bit();

            if (id_bit != cmp_id_bit) { // if the bit and its complement are not the same there was no conflict and we can simply use id_bit
                // 11
                search_direction = id_bit;  // bit write value for search
            // 8
            } else if (id_bit == 0) { // if both bits are zeroes we have to check further
                // 9
                if (id_bit_number == last_discrepancy) { // if our current bit is the same we had the last discrepancy at then search_direction is 1
                    search_direction = 1; // 12
                } else { 
                    // 10
                    if (id_bit_number > last_discrepancy) { // if our current bit is more significant than the one we had the last discrepancy at then search_direction is 0
                        search_direction = 0; // 13
                    } else { // if the current bit is less siginificant than the one we had the last discrepancy at look up what stands in the current bit position of the current byte
                        search_direction = ((ROM_NO[rom_byte_number] >> bit_position) & 0x01); // 14
                    }
                }
                // 16
                if (search_direction == 0) { // update last_zero if search_direction is zero
                    last_zero = id_bit_number; // 17

                    // 19
                    if (last_zero < 9) // if the last zero was in the family code of the rom note it in last_family_discrepancy
                        last_family_discrepancy = last_zero; // 20
                }
            // 7
            } else if (id_bit == 1) { // if both bits are zeroes it means there is no sensor connected so we reset the search // TODO maybe just break out of the loop?
                // 26
                last_discrepancy = 0;
                last_device_flag = false;
                last_family_discrepancy = 0;
                return false; // 27
            }
            printf("Gesendetes Bit: %d\n", search_direction);
            
            byte |= (search_direction << bit_position++); // 15; write search_direction into the byte
            write_bit(search_direction); // write search_direction as confirmation for the sensors
            
            if (bit_position > 7) { // check if we reached the end of the byte
                ROM_NO[rom_byte_number++] = byte; // set the byte to the current byte in ROM_NO and increment rom_byte_number
                byte = 0; // reset byte
                bit_position = 0; // reset bit_position
            }
        } // 21; end of for-loop
        last_discrepancy = last_zero; // 22; after the for loop is finished we note the last discrepancy

        // if there was not discrepancy it means this was the last device
        if (last_discrepancy == 0) { // 23
            last_device_flag = true; // 24
            printf("Last device flag set");
        }
    }    
    printf("last_zero: %d, last_discrepancy: %d\n", last_zero, last_discrepancy);

    // 25
    if (check_crc(ROM_NO, ROM_SIZE)) { // if crc check succeeds add the rom to the roms list
        printf("Rom: ");
        for (int i = 0; i < ROM_SIZE; i++) {
            roms[roms_index][i] = ROM_NO[i];
            printf("%x", ROM_NO[i]);
        }
        printf("\n\n");
        roms_index++;
    } else { // 26; if the crc check does not succeeds reset the search
        last_discrepancy = 0;
        last_device_flag = false;
        last_family_discrepancy = 0;
        return false; // 27
    }
    return true; // 28
    // 29
}


void detect_sensors_and_mesaure_temperature() { // step 3
    first(); // initialize search
    while (!last_device_flag) { // search until last device was found
        next();
    }
    for (int i = 0; i < roms_index; i++) {
        measure_temperature_specific_sensor(roms[i]);
		TFT_newline();
		TFT_carriage_return();
    }
	wait(DELAY_RATE);
	TFT_cls();
	TFT_gotoxy(1,1);
}

void superloop(void) {
    // one-wire initialization
    set_mode_push_pull(1);
    set_mode_open_drain(0);
    
    while(1) {
        //GPIOG -> ODR = GPIOG -> IDR | (0x1U << 3); // turn LED 3 on
        //read_rom(); // step 1
        //measure_temperature_one_sensor(); // step 2.1
        //measure_temperature_specific_simulation_sensors(); // step 2.2
        detect_sensors_and_mesaure_temperature(); // step 3
        //GPIOG -> ODR = GPIOG -> IDR & ~(0x1U << 3);  // turn LED 3 off
    }
}
