/**
  ******************************************************************************
  * @file    	main.c 
  * @author  	Philipp Schwarz
  *        	    HAW-Hamburg
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2018-05-15
  * @brief   Main program body
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "TI_Lib.h"
#include "timer.h"
#include "control.h"

/**
  * @brief  Main program
  * @param  None
  */
int main(void) {
    Init_TI_Board();
    timerinit();
    TFT_Init();
    TFT_cls();
    superloop();
    return 0;
}
// EOF
