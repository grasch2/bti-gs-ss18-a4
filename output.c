/**
  ******************************************************************************
  * @file    	output.c 
  * @author  	Philipp Schwarz
  *        	    HAW-Hamburg
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2018-05-15
  * @brief   Main program body
  ******************************************************************************
  */
  
#include <stdio.h> // for snprintf()
#include "TI_Lib.h" //for get_uptime();
#include <stdbool.h> // for booleans

#define DELAY_RATE 200 // ms
#define MAX_TEMP 125.0F
#define MIN_TEMP -55.0F
#define ERROR_TEMP 85.0F

uint32_t timeD = 0;
uint32_t prevTimeD = 0;

char text[64];

void display_rom_data(uint8_t* rom) {
    timeD = get_uptime();
    if (timeD - prevTimeD > DELAY_RATE) {
        uint64_t ser_num = 0;
        
        TFT_cls();
        
        for (int i = 1; i < 7; i++) {
            ser_num = ser_num << 8;
            ser_num |= rom[i];
        }
        
        snprintf(text, 64, "Fam Code: %x\n\rSer Num : %llx\n\rCRC     : %x", rom[0], ser_num, rom[7]);
        TFT_puts(text);
        
        prevTimeD = timeD;
    }
}

void display_error_message(int e) {
    TFT_set_font_color(RED);
    TFT_cls();
    switch (e) {
    case 0:
        break;
    case -1:
        TFT_puts("CRC check failed");
        break;
    case -2:
        TFT_puts("No sensor connected");
        break;
    case -3:
        TFT_puts("Temperature out of range");
        break;
    case -5:
        TFT_puts("Measurement error");
        break;
    default:
        TFT_puts("Unknown Error");
    }
    if (e != 0) {
        Delay(2000);
    }
    TFT_cls();
    TFT_set_font_color(WHITE);
}

void display_temperature(uint8_t t1, uint8_t t2) {
    int16_t pre_temp = t1 << 8 | t2;
    float temperature = (float) pre_temp / 16;
    
    // untested without float cast
    /*if (pre_temp < 0) {
        pre_temp = ~pre_temp + 1;
    }
    uint8_t decimal = pre_temp & 0x000F;
    int16_t temp_without_decimal = pre_temp >> 4;*/
    
    // old "bad" version
    /*uint16_t fixed_point_numbers[4] = {625, 1250, 2500, 5000};
    
    int16_t temperature = 0;
    uint8_t pre_decimal = 0;
    uint16_t decimal = 0;
    
        pre_decimal = t2 & 0x0F; // only the last 4 bit are relevant for the decimal point
        for (int i = 0; i < 4; i++) {
            if (((pre_decimal >> i) & 0x01) == 1) { // shift pre_decimal and look at the lsb, if it is one add the respective number from the fixed_point_numbers array to decimal
                decimal += fixed_point_numbers[i];
            }
        }
        
        temperature = (int16_t) t1 << 8 ; // shift the value 8 digits left so the msb of t1 is also the msb of temperature (for detecting negative numbers)
        temperature = temperature >> 4; // shift it back 4 bits because we have a 12 bit number
        temperature |= t2 >> 4; // shift t2 so the decimal gets away and or it with temperature (the 4 digits we or with are 0s in temperature)
        
        if (temperature < 0 && decimal > 0) { // if we have a temperature below zero with digits after the decimalpoint we have to subtract the decimal
            temperature += 1;
            decimal = 10000 - decimal;
        }*/
    
        
        if (MIN_TEMP <= temperature && temperature <= MAX_TEMP) { // check if the temperature is in range of the specification
            if (temperature == ERROR_TEMP) { // check if we got the measurement error temperature
                display_error_message(-5);
            } else {
                //TFT_cls();
                snprintf(text, 64, "Temperature: %f %cC", temperature, 248); // 248 for �
                TFT_puts(text);
            }
        } else { // the temperature is out of range so output an error message
            display_error_message(-3);
        }
}
