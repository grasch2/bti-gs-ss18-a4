/**
  ******************************************************************************
  * @file    	one-wire.h 
  * @author  	Philipp Schwarz
  *        	    HAW-Hamburg
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2018-05-15
  * @brief   Main program body
  ******************************************************************************
  */

#include <stdint.h> // for standarized integers

typedef enum Voltage {
    LOW,
    HIGH
} voltage;

/**
 * Returns the bus state
 * @return 1 if the bus is high, 0 if it is low
 */
unsigned char get_bus_state(void);

/**
 * Reads a bit from the bus
 * @return The read bit
 */
unsigned char read_bit(void);

/**
 * Reads a byte from the bus
 * @return The read byte
 */
unsigned char read_byte(void);

/**
 * Resets the bus
 * @return A 0 if atleast one sensor has been detected or if not a 1
 */
unsigned char reset_bus(void);

/**
 * Sets the voltage on a pin
 * @param v The voltage, either high or low
 * @param pin The pin we are going to set the voltage on
 */
void set_pin_voltage(voltage v, uint8_t pin);

/**
 * Sets the mode on the pin to open drain
 * @param pin The pin we are going to set
 */
void set_mode_open_drain(uint8_t pin);

/**
 * Sets the mode on the pin to push pull
 * @param pib The pin we are going to set
 */
void set_mode_push_pull(uint8_t pin);

/**
 * Writes a bit on the bus
 * @param bit The bit we are going to write
 */
void write_bit(unsigned char bit);

/**
 * Writes a byte on the bus
 * @param byte The byte we are going to write
 */
void write_byte(unsigned char byte);
