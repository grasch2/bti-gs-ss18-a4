/**
  ******************************************************************************
  * @file    	main.h 
  * @author  	Philipp Schwarz
  *        	    HAW-Hamburg
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2018-05-15
  * @brief   Main program body
  ******************************************************************************
  */

/**
 * Initializes the TI-C-Board and starts the superloop of control.c
 * @return a 0 if everything succeeds (since the superloop is an infinite loop we will never reach the return)
 */
int main(void);
